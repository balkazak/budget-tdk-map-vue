import Vue from 'vue'
import App from './App.vue'
import Buefy from 'buefy'
import './assets/scss/app.scss'
import router from "./services/router";
import store from "./services/store";
import axiosInstance from "./services/axios";
import 'leaflet/dist/leaflet.css';
import 'bootstrap/dist/css/bootstrap.min.css'

Vue.use(Buefy,{
  defaultIconPack: 'fas'
});

Vue.config.productionTip = false

let vueInstance = new Vue({
  router,
  store,
  axiosInstance,
  render: function (h) { return h(App) }
}).$mount('#app')

router.app = vueInstance;
