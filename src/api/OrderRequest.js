import axios from '@/services/axios'
import store from '@/services/store'
import qs from 'qs'
import querystring from 'querystring'

const OrderRequest = {
  createEmptyOrder() {
    return axios.get('/rest/api/registry/create_doc', {
      params: {
        registryCode: 'podacha_zayavki',
      },
    })
  },
  getFormDataFormat(uuid) {
    return axios.get('/rest/api/asforms/data/get', {
      params: {
        dataUUID: uuid,
      },
    })
  },
  saveForm(form, uuid) {
    let queries = qs.stringify({ data: '"data":' + JSON.stringify(form)});
  
    let url =
      '/rest/api/asforms/data/save'
    //   '/rest/api/asforms/data/merge'
    ;
    
    var config = {
        method: 'post',
        url: url + '?uuid=' + uuid,
        headers: { 
          'Authorization': store.getters['authorization/getAuthorization'], 
        },
        data : queries
      };
      
      return axios(config);
  },
  activateForm(uuid) {
    return axios.get('/rest/api/registry/activate_doc', {
      params: {
        dataUUID: uuid,
      },
    })
  },
  saveFile(uuid, nodeUUID, file) {
    let form = new FormData()
    form.append('file', file)
    return axios.post('/rest/api/asffile', form, {
      headers: {
        Authorization: store.getters['authorization/getAuthorization'],
        'Content-Type': 'multipart/form-data',
      },
      params: {
        type: 'attachment',
        node: nodeUUID,
        data: uuid,
      },
    })
  },
  getFileDescription(fileId) {
    return axios.get('/rest/api/storage/description', {
      headers: {
        // 'Content-type': 'application/json',
        Authorization: store.getters['authorization/getAuthorization'],
      },
      params: {
        elementID: fileId,
      },
    })
  },
  getFormByFormId(formId) {
    return axios.get('/rest/api/asforms/data/' + formId, {
      headers: {
        'Content-type': 'application/json',
        Authorization: store.getters['authorization/getAuthorization'],
      },
    })
  },
  getOrdersList() {
      return axios.get('rest/api/registry/data_ext', {
        params: {
            registryCode: 'podacha_zayavki'
        },
        headers: {
            Authorization: store.getters['authorization/getAuthorization'],
        }
      })
  }
}

export { OrderRequest }
