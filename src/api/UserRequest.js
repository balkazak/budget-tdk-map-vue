import axios from '@/services/axios';
import sha1 from 'sha1';
import store from '@/services/store';

const UserRequest = {

    updateOrCreate(data) {
        let params = {
            locale: 'ru',
            isAdmin: data.admin,
            firstname: data.firstname,
            lastname: data.lastname,
            patronymic: data.patronymic,
            email: data.mail,
            jid: data.phone,
            login: data.mail,
            password: sha1(data.pass),
            pointersCode: data.iin
        };

        if (data.userid) {
            params.userID = data.userid;
        }

        let queryParams = new URLSearchParams();

        for (let param in params) {
            queryParams.append(param, params[param]);
        }

        return axios.post('/rest/api/filecabinet/user/save', queryParams);
    },
    getExtProfileInfo(userId, login, pass) {
        return axios.get('rest/api/registry/data_ext', {
            headers: {
                Authorization: store.getters['authorization/getAuthorization'],
            },
            params: {
                registryCode:'lichnyi_kabinet',
                fields: [
                    'fio',
                    'familia',
                    'otchestvo',
                    'IIN',
                    'telephon',
                    'email',
                    'address'
                ],
                field: 'userId',
                condition:'TEXT_EQUALS',
                value: userId
            }
        });
    },
    putExtProfileInfo(form, uuid) {
        return axios.post('rest/api/asforms/data/merge', {
            headers: {
                Authorization: store.getters['authorization/getAuthorization'],
                "Content-Type": "application/json"
            },
            params: {
                "uuid": uuid,
                "data": [
                    { id: 'fio', type: 'textbox', key: '', value: fio },
                    { id: 'familia', type: 'textbox', key: "", value: form.lastname },
                    { id: 'otchestvo', type: 'textbox', key: "", value: form.patronymic },
                    { id: 'IIN', type: 'textbox', key: "", value: form.iin } ,
                    { id: 'telephon', type: 'textbox', key: "", value: form.phone },
                    { id: 'email', type: 'textbox', key: "", value: form.mail },
                    { id: 'address', type: 'textarea', key: "", value: form.address }
                ]
            }
        })
    },
    addUserToGroup(userId) {
        return axios.get('/rest/api/storage/groups/add_user', {
            params: {
                groupCode: process.env['VUE_APP_API_GROUP_ID'],
                locale: 'ru',
                userID: userId
            }
        });
    },
    authorize(login, pass) {
        return axios.get('/rest/api/person/auth', {
            headers: {
                Authorization: 'Basic ' + new Buffer(login + ':' + sha1(pass)).toString('base64')
            }
        });
    }
};

export { UserRequest };