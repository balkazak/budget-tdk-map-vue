import Vue from 'vue';
import Vuex from 'vuex';
import { user, authorization } from "../store";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {    },
    mutations: {    },
    actions: {    },
    modules: {
        user,
        authorization
    }
});