import routes from "../routes";
import Vue from 'vue'
import VueRouter from 'vue-router';
import store from './store';

Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  routes,
  linkActiveClass: "is-active",
  base: process.env.BASE_URL
});

router.beforeEach((to, from, next) => {
  if (to.meta.isNeedAuthorized && !store.getters['user/getUser']) {
    next({ name: 'login' });
  } else {
    next();
  }
})

export default router;