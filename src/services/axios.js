import axios from "axios";

let axiosInstance;
if (axiosInstance === undefined) {
  axiosInstance = axios.create({
    baseURL: process.env["VUE_APP_API_BASE_URL"],
    timeout: 0,
    headers: {
      Authorization: 'Basic ' + window.btoa('$key:' + process.env["VUE_APP_API_TOKEN"]),
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  });
}

export default axiosInstance;
