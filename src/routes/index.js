import Home from '@/views/Home';
import UserRegistration from '@/views/UserRegistration';
import Projects from '@/views/Projects';
import Profile from '@/views/Profile';
import Project from '@/views/Project';
import ProjectOrder from '@/views/ProjectOrder';
import Map from '@/views/Map';


export default [
    {
        path: "/",
        name: "home",
        component: Home
    },
    {
        path: "/login",
        name: "login",
        component: UserRegistration
    },
    {
        path: "/projects",
        name: "projects",
        component: Projects
    },
    {
        path: "/profile",
        name: "profile",
        component: Profile,
        meta: {
            isNeedAuthorized: true
        }
    },
    {
        path: "/project",
        name: "project",
        component: Project,
        meta: {
            isNeedAuthorized: true
        }
    },
    {
        path: "/project-order",
        name: "project-order",
        component: ProjectOrder,
        meta: {
            isNeedAuthorized: true
        }
    },
    {
        path: "/map",
        name: "map",
        component: Map
    }
];
