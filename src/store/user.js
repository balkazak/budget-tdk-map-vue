export default {
    namespaced: true,
    state: {
        user: JSON.parse(localStorage.getItem('user'))
    },
    getters: {
        getUser: state => state.user,
    },
    actions: {
        setUser(context, user) {
            if (user.pass && user.pass.length > 0) {
                user.pass = null;
            }
            
            if (user.passCheck && user.passCheck.length > 0) {
                delete user.passCheck;
            }
            context.commit('setUser', user);
        }
    },
    mutations: {
        setUser(state, user) {
            localStorage.setItem('user', JSON.stringify(user));
            state.user = user;
        }
    },
};