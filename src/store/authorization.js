import sha1 from 'sha1';

export default {
    namespaced: true,
    state: {
        authorization: localStorage.getItem('authorization')
    },
    getters: {
        getAuthorization: state => state.authorization
    },
    actions: {
        setAuthorization(context, {login, pass}) {
            let autorization = 'Basic ' + new Buffer(login + ':' + sha1(pass)).toString('base64');
            context.commit('setAuthorization', autorization);
        }
    },
    mutations: {
        setAuthorization(state, authorization) {
            localStorage.setItem('authorization', authorization);
            state.authorization = authorization;
        }
    }
};